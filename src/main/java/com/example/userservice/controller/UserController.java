package com.example.userservice.controller;

import com.example.userservice.dto.UserDepartment;
import com.example.userservice.entity.MyUser;
import com.example.userservice.repo.UserRepo;
import com.example.userservice.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/user")
public class UserController {

    @Value("${external.service.department}")
    private String departmentServiceEP;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping
    public MyUser save(@RequestBody MyUser user){
        return userRepo.save(user);
    }

    @GetMapping("/{id}")
    public UserVo getById(@PathVariable Long id){
        UserVo userVO = new UserVo();
        MyUser user = userRepo.findById(id).get();
        UserDepartment userDepartment = restTemplate.getForObject( departmentServiceEP+ user.getDepartmentId(), UserDepartment.class);
        userVO.setUser(user);
        userVO.setDepartment(userDepartment);
        return userVO;
    }

}
