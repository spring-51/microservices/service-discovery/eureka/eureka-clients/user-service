package com.example.userservice.repo;

import com.example.userservice.entity.MyUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<MyUser, Long> {
}
