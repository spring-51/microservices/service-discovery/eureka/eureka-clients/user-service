package com.example.userservice.vo;

import com.example.userservice.dto.UserDepartment;
import com.example.userservice.entity.MyUser;

public class UserVo {

    private MyUser user;
    private UserDepartment department;

    public MyUser getUser() {
        return user;
    }

    public void setUser(MyUser user) {
        this.user = user;
    }

    public UserDepartment getDepartment() {
        return department;
    }

    public void setDepartment(UserDepartment department) {
        this.department = department;
    }
}
